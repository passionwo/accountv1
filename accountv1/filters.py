from django import template

register = template.Library()


@register.filter(name='none_filter')
def none_filter(value):
    if not value:
        return '--'
    return value


def file_datetime(value):
    if not value:
        return '--'

    t = value.split('-')

    return '/'.join(t)
