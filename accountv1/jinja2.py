from django.templatetags.static import static
from django.urls import reverse

from jinja2 import Environment
from .filters import none_filter, file_datetime


def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': static,
        'url': reverse,
    })
    env.filters.update({
        'none_filter': none_filter,
        'file_datetime': file_datetime,
    })
    return env
