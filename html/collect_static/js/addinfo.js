$(function(){
    $("#upload-form").submit(function(e){
        e.preventDefault();

        $(this).ajaxSubmit({
            url: "/project/upload/",
            data: $(this).serialize(),
            method: "POST",
            headers: {
                "Authorization": "Token " + localStorage.getItem("token"),
            },
            success: function(data, textStatus, xhr){
                alert(data.detail);
                location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status == 400){
                    alert(jqXHR.responseJSON.detail);
                }else if(jqXHR.status == 500){
                    alert("服务器出错");
                }else if(jqXHR.status == 401 || jqXHR.status == 403){
                    alert(jqXHR.responseJSON.detail);
                    window.open("/", "_self");
                }
            }
        });
    });

    $("#btn-info-add").click(function(){
        var data = $("#add-info-form").serialize();
        $.ajax({
            url: "/project/createinfo/",
            type: "post",
            data: data,
            headers: {
                "Authorization": "Token " + localStorage.getItem("token"),
            },
            success: function(resp, txt, xhr){
                alert(resp.detail);
                show_info();
            },
            error: function(jqXHR, txt, status){
                if(jqXHR.status == 401 || jqXHR.status == 403){
                    alert(jqXHR.responseJSON.detail);
                    window.open("/", "_self");
                }else{
                    alert(jqXHR.responseJSON.detail);
                }
            }
        });
    });
    $("#btn-info-update").click(function(){
        var data = $("#add-info-form").serialize();
        $.ajax({
            url: "/project/createinfo/",
            type: "post",
            data: data,
            headers: {
                "Authorization": "Token " + localStorage.getItem("token"),
            },
            success: function(resp, txt, xhr){
                alert(resp.detail);
                show_info();
            },
            error: function(jqXHR, txt, status){
                if(jqXHR.status == 401 || jqXHR.status == 403){
                    alert(jqXHR.responseJSON.detail);
                    window.open("/", "_self");
                }else{
                    alert(jqXHR.responseJSON.detail);
                }
            }
        });
    });
})


function clear_form(){
    $("#add-info-form").trigger("reset");
}