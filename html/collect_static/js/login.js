$(function(){
    $(".to_change").click(function(){
        $(".to_login").show();
        $(".to_change").hide();
        $(".login_module").hide();
        $(".change_module").show();
    });
    $(".to_login").click(function(){
        $(".to_login").hide();
        $(".to_change").show();
        $(".login_module").show();
        $(".change_module").hide();
    });

    login();
    change_pwd();

    close_alter();

    $(".login_form").validate({
        rules: {
            email: {
                required: true,
                minlength: 2,
                maxlength: 40,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 18,
            },
        },
        errorPlacement: function(error, element) {  
            element.parent().next().show();
            error.appendTo(element.parent().next().children());  
        },
    });

    $(".change_form").validate({
        rules: {
            email: {
                required: true,
                minlength: 2,
                maxlength: 40,
            },
            code: {
                required: true,
                minlength: 6,
                maxlength: 6,
            },
            new_password: {
                required: true,
                minlength: 6,
                maxlength: 18,
            },
            verify_password: {
                required: true,
                minlength: 6,
                maxlength: 18,
            },
        },
        errorPlacement: function(error, element) {  
            element.parent().next().show();
            error.appendTo(element.parent().next().children());  
        },
    });


})


function login(){
    $('.login_form').submit(function(e){
        e.preventDefault();

        var is_ok = $(".login_form").valid();

        if(is_ok){
            $.ajax({
                url: "/",
                method: 'POST',
                data: $(this).serialize(),   
                success:function (resp, textStatus, xhr) {
                    localStorage.setItem('token', resp.token);
                    window.open("/project/index/", "_self");
                },
                error:function(xhr, txt, error){
                    var errmsg = xhr.responseJSON.detail;
                    $(".error-msg").html(xhr.responseJSON.detail);
                    $(".login-error").show();
                }
            })
        }
    });
}

function send_code(){
    var t = 60;
    var is_ok = $(".email-user").valid();

    if(is_ok){
        var email = $(".email-user").val();

        $.ajax({
            url: "/verify/" + email + "/",
            method: 'GET',
            success:function (resp, textStatus, xhr) {
            },
            error:function(xhr, txt, error){
                var errmsg = xhr.responseJSON.detail;
                $(".error-msg").html(xhr.responseJSON.detail);
                $(".change-error").show();
            }
        });
    }   
    $(".btn-get-code").removeAttr("onclick");
    var codeVar = setInterval(function(){
        t--;
        var ti = t.toString();
        $(".btn-get-code").html("重新验证&nbsp;&nbsp;" + t + "s");
        if(t == 0){
            clearInterval(codeVar);
            $(".btn-get-code").html("获取验证码");
            $(".btn-get-code").attr("onclick", "send_code()")
        }
    }, 1000);
}

function change_pwd(){
    $(".change_form").submit(function(e){
        e.preventDefault();

        var is_ok = $(".change_form").valid();
        if(is_ok){
            var data = $(".change_form").serialize();
            $.ajax({
                url: "/password/",
                method: "POST",
                data: data,
                success: function(resp, txt, xhr){
                    location.reload();
                },
                error: function(jqXHR, txt, status){
                    $(".error-msg").html(jqXHR.responseJSON.detail);
                    $(".change-error").show();
                }

            });
        }
    });
}


function close_alter(){
    $(".close").click(function(){
        $(this).parent().hide();
    });
}