$(function(){
    var url = window.location.href;
    var arr = url.split("/");

    $.ajax({
        url: '/project/info/'+arr[arr.length-2]+'/info_detail/',
        headers: {
            "Authorization": "Token " + localStorage.getItem("token"),
        },
        success: function(resp){
            var keys_array = (Object.keys(resp));
            for(var i=0;i<keys_array.length;i++){
                var k = keys_array[i];
                if(resp[k] == null){
                    resp[k] = '--';
                }
            }

        var innertxt = ''+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">档案编号</td>'+
            '<td class="info-detail-td two-active">'+resp.doc_num+'</td>'+
            '<td class="info-detail-td one-active">档案位置</td>'+
            '<td class="info-detail-td two-active">'+resp.doc_index+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">文件题名</td>'+
            '<td class="info-detail-td two-active">'+resp.title+'</td>'+
            '<td class="info-detail-td one-active">标的</td>'+
            '<td class="info-detail-td two-active">'+resp.subtitle+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">档案类型</td>'+
            '<td class="info-detail-td two-active">'+resp.file_type+'</td>'+
            '<td class="info-detail-td one-active">责任者2</td>'+
            '<td class="info-detail-td two-active">'+resp.op_site+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">责任者1</td>'+
            '<td class="info-detail-td two-active">'+resp.file_main+'</td>'+
            '<td class="info-detail-td one-active">文件日期</td>'+
            '<td class="info-detail-td two-active">'+resp.start_time+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">到期时间</td>'+
            '<td class="info-detail-td two-active">'+resp.end_time+'</td>'+
            '<td class="info-detail-td one-active">归档部门</td>'+
            '<td class="info-detail-td two-active">'+resp.arch_department+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">归档时间</td>'+
            '<td class="info-detail-td two-active">'+resp.arch_time+'</td>'+
            '<td class="info-detail-td one-active">归档人</td>'+
            '<td class="info-detail-td two-active">'+resp.resp_person+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">数量</td>'+
            '<td class="info-detail-td two-active">'+resp.page_num+'</td>'+
            '<td class="info-detail-td one-active">概述</td>'+
            '<td class="info-detail-td two-active">'+resp.file_content+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">密级</td>'+
            '<td class="info-detail-td two-active">'+resp.file_secret+'</td>'+
            '<td class="info-detail-td one-active">保管期限</td>'+
            '<td class="info-detail-td two-active">'+resp.time_long+'</td>'+
        '</tr>'+
        '<tr class="detail-tr">'+
            '<td class="info-detail-td one-active">原件</td>'+
            '<td class="info-detail-td two-active">'+resp.is_origin+'</td>'+
            '<td class="info-detail-td one-active">备注</td>'+
            '<td class="info-detail-td two-active">'+resp.text_msg+'</td>'+
        '</tr>';
        $('.detail-table').append(innertxt);
        func();
        },
        error: function(jqXHR, txt, status){
            if(jqXHR.status == 401 || jqXHR.status == 403){
                alert(jqXHR.responseJSON.detail);
                window.open("/", "_self");
            }else{
                alert(jqXHR.responseJSON.detail);
            }
        }
    });

})

function func(){
    $(".one-active").hover(
    function(){
        $(this).addClass("css-hover-o");
        $(this).next().addClass("css-hover-o");
    }, function(){
        $(this).removeClass("css-hover-o");
        $(this).next().removeClass("css-hover-o");
    });

    $(".two-active").hover(
    function(){
        $(this).addClass("css-hover-o");
        $(this).prev().addClass("css-hover-o");
    }, function(){
        $(this).removeClass("css-hover-o");
        $(this).prev().removeClass("css-hover-o");
    });
    $(".two-active").dblclick(
        function(){
            var selection = window.getSelection();        
            var range = document.createRange();
            // 注意: 是this 不是节点
            range.selectNodeContents(this);
            selection.removeAllRanges();
            selection.addRange(range);
            var is_success = document.execCommand("copy");
            if(is_success){
                alert("复制成功");
            }else{
                return;
        }
    });
}