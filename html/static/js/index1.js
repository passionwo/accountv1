$(function(){

    $("#select_file_start_date").datepicker({
        dateFormat: "yy-mm-dd",
        yearRange: '2000:c',
        changeMonth: true,
        changeYear: true
    });
    $("#select_file_end_date").datepicker({
        dateFormat: "yy-mm-dd",
        yearRange: '2000:c',
        changeMonth: true,
        changeYear: true
    });

    function ajax_post_page(post_url){
        $.ajax({
            url: post_url, 
            type: "post",
            data: $("#form_to_select").serialize(),
            headers: {
                "Authorization": "Token " + localStorage.getItem("token"),
            },
            success: function(resp, txt, xhr){
                inner_func(resp, txt, xhr, "select");
                clear_select_form();
            },
            error: function(jqXHR, txt, status){
                if(jqXHR.status == 401 || jqXHR.status == 403){
                    alert(jqXHR.responseJSON.detail);
                    window.open("/", "_self");
                }else{
                    alert(jqXHR.responseJSON.detail);
                }
            }
        })
    }

    ajax_page("/project/info/")

    $("#form_to_select").submit(function(e){
        e.preventDefault();
        localStorage.removeItem("select_info");
        localStorage.setItem("select_info", $("#form_to_select").serialize());
        ajax_post_page("/project/select_info/");
    });
})

function ajax_post_page(post_url){
    var info =  localStorage.getItem("select_info");
    $.ajax({
        url: post_url, 
        type: "post",
        data: info,
        headers: {
            "Authorization": "Token " + localStorage.getItem("token"),
        },
        success: function(resp, txt, xhr){
            inner_func(resp, txt, xhr, "select");
            clear_select_form();
        },
        error: function(jqXHR, txt, status){
            if(jqXHR.status == 401 || jqXHR.status == 403){
                alert(jqXHR.responseJSON.detail);
                window.open("/", "_self");
            }else{
                alert(jqXHR.responseJSON.detail);
            }
        }
    })
}

function export_file(){
    var is_sure = confirm("是否下载当前查询记录?");
    if(is_sure){
        $.ajax({
            url: "/project/export/", 
            type: "get",
            headers: {
                "Authorization": "Token " + localStorage.getItem("token"),
            },
            success: function(resp, txt, xhr){
                window.open(resp.detail, "_blank");
            },
            error: function(jqXHR, txt, status){
                if(jqXHR.status == 401 || jqXHR.status == 403){
                    alert(jqXHR.responseJSON.detail);
                    window.open("/", "_self");
                }else{
                    alert(jqXHR.responseJSON.detail);
                }
            }
        })
    }
}

function clear_form(){
    $("#add-info-form").trigger("reset");
}

function clear_select_form() {
    $("#form_to_select").trigger("reset");
}

function show_info(){
    ajax_page("/project/info/");
    $(".nav-1").addClass("active");
    $(".nav-2").removeClass("active");
    $(".upload-file-tab").hide();
    $(".info-tab-module").show();
    $(".own-write-tab").hide();
}

function show_upload_info(){
    $(".nav-2").addClass("active");
    $(".nav-1").removeClass("active");
    $(".upload-file-tab").show();
    $(".info-tab-module").hide();
    $(".own-write-tab").hide();
}

function show_add_info(stat){
    $(".nav-2").addClass("active");
    $(".nav-1").removeClass("active");
    $(".own-write-tab").show();
    $(".upload-file-tab").hide();
    $(".info-tab-module").hide();

    if(stat == "edit"){
        $(".edit-way").show();
        $(".info-way").hide();
        $("#btn-info-update").show();
        $("#btn-info-add").hide();
        $(".content-table-head").text("信息更新");
        $("#btn-to-clear-add").hide();
    }else{
        clear_form();
        $(".edit-way").hide();
        $(".info-way").show();
        $("#btn-info-update").hide();
        $("#btn-info-add").show();
        $(".content-table-head").text("填写表格");
        $("#btn-to-clear-add").show();
    }
}


function page_func(innertxt, cur_page, total_page, tps){
    $("#pagination").pagination({
        dataSource: innertxt,
        showGoInput: true,
        showGoButton: true,
        currentPage: cur_page,
        totalPage: total_page,
        callback: function(current) {
            if(tps=="normal"){
                ajax_page("/project/info/"+current+"/");
            }else{
                ajax_post_page("/project/select_info/"+current+"/");
            }
        }
    });
}

function sign_out(){
    localStorage.clear();
    window.location.href = "/";
}

function alert_delete_info(value, cur_page){
    var is_sure = confirm("是否确认删除?");
    if(is_sure){
        $.ajax({
            url: "/project/info/" + value + "/info_delete/",
            headers: {
                "Authorization": "Token " + localStorage.getItem("token"),
            },
            method: "GET",
            success: function(data, textStatus, xhr){
                if(xhr.status === 204){
                    alert("删除成功");
                    ajax_page("/project/info/"+cur_page+"/")
                }else{
                    alert("出现错误...");
                }
            },
            error:function(jqXHR, txt, status){
                if(jqXHR.status == 401 || jqXHR.status == 403){
                    alert(jqXHR.responseJSON.detail);
                    window.open("/", "_self");
                }else{
                    alert(jqXHR.responseJSON.detail);
                }
            }
        });
    }
}

function ajax_page(url){
    $.ajax({
        url: url,
        headers: {
            "Authorization": "Token " + localStorage.getItem("token"),
        },
        success: function(resp, txt, xhr){
            inner_func(resp, txt, xhr, 'normal');
        },
        error: function(jqXHR, txt, status){
            if(jqXHR.status == 401 || jqXHR.status == 403){
                alert(jqXHR.responseJSON.detail);
                window.open("/", "_self");
            }else{
                alert(jqXHR.responseJSON.detail);
            }
        }
    });

}

function inner_func(resp, txt, xhr, tps){
    $(".user-name").html(resp.username);
            
    var innertxt = ['<thead><tr class="head-info">'+
       '<td class="info-td" scope="col">档案编号</td>'+
       '<td class="info-td" scope="col">档案类型</td>'+
       '<td class="info-td" scope="col">文件题名</td>'+
       '<td class="info-td" scope="col">责任者1</td>'+
       '<td class="info-td" scope="col">责任者2</td>'+
       '<td class="info-td" scope="col">文件日期</td>'+
       '<td class="info-td" scope="col">数量</td>'+
       '<td colspan="3"></td>'+
   '</tr></thead>',];
    var info_list = resp.list_of_info;
    for(var i=0;i < info_list.length; i++){
        // null 过滤
        var keys_array = (Object.keys(info_list[i]));
            for(var m=0;m<keys_array.length;m++){
                var k = keys_array[m];
                if(info_list[i][k] == null){
                    info_list[i][k] = '--';
            }
        }

        if(resp.has_del_permission){
            innertxt.push('<tr class="info-tr">'+
                '<td class="info-td">'+ info_list[i].doc_num +'</td>'+
                '<td class="info-td">'+ info_list[i].file_type +'</td>'+
                '<td class="info-td">'+ info_list[i].title +'</td>'+
                '<td class="info-td">'+ info_list[i].file_main +'</td>'+
                '<td class="info-td">'+ info_list[i].op_site +'</td>'+
                '<td class="info-td">'+ info_list[i].start_time +'</td>'+
                '<td class="page-col">'+ info_list[i].page_num +'</td>'+
                '<td class="info-td op-td" colspan="3">'+
                  '<div class="dropdown show"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">操作</a>'+
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">'+
                        '<a href="javascript:void(0);" onclick="detail_account('+info_list[i].id+')" class="dropdown-item">'+
                            '<img src="/static/img/detail.png" alt="" class="info-detail-img"  width="30" height="30">'+
                            '详情'+
                        '</a>'+
                        '<a href="javascript:void(0);" onclick="edit_account('+info_list[i].id+')" class="dropdown-item">'+
                            '<img src="/static/img/detail.png" alt="" class="info-detail-img" width="30" height="30">'+
                            '编辑'+
                        '</a>'+
                        '<a href="javascript:void(0);" onclick="alert_delete_info('+info_list[i].id+', '+resp.cur_page+')" class="data-del-a dropdown-item">'+
                        '<img src="/static/img/detail.png" alt="" class="info-detail-img"  width="30" height="30">'+
                        '删除'+
                        '</a>'+
                    '</div></div>'+
                '</td>'+
        '</tr>');
        }else{
            innertxt.push('<tr class="info-tr">'+
                '<td class="info-td">'+ info_list[i].doc_num +'</td>'+
                '<td class="info-td">'+ info_list[i].file_type +'</td>'+
                '<td class="info-td">'+ info_list[i].title +'</td>'+
                '<td class="info-td">'+ info_list[i].file_main +'</td>'+
                '<td class="info-td">'+ info_list[i].op_site +'</td>'+
                '<td class="info-td">'+ info_list[i].start_time +'</td>'+
                '<td class="info-td">'+ info_list[i].page_num +'</td>'+
                '<td class="info-td op-td" colspan="3">'+
                    '<div class="dropdown show"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">操作</a>'+
                    '<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">'+
                        '<a href="javascript:void(0);" onclick="detail_account('+info_list[i].id+')" class="dropdown-item">'+
                            '<img src="/static/img/detail.png" alt="" class="info-detail-img"  width="30" height="30">'+
                            '详情'+
                        '</a>'+
                        '<a href="javascript:void(0);" onclick="edit_account('+info_list[i].id+')" class="dropdown-item">'+
                            '<img src="/static/img/detail.png" alt="" class="info-detail-img" width="30" height="30">'+
                            '编辑'+
                        '</a>'+
                    '</div></div>'+
                '</td>'+
            '</tr>');
        }
    }
    $(".info-table").html("<tbody>" + innertxt + "</tbody>");
    page_func(innertxt, resp.cur_page, resp.total_pages, tps);
}

// 详情
function detail_account(cur_id){
    window.open(
        "/project/detail_info/"+cur_id+"/",
        '_blank'
    );
}

function form_info(cur_id){
    $.ajax({
        url: '/project/info/'+cur_id+'/info_detail/',
        headers: {
            "Authorization": "Token " + localStorage.getItem("token"),
        },
        success: function(resp){
            var keys_array = (Object.keys(resp));
            for(var i=0;i<keys_array.length;i++){
                var k = keys_array[i];
                if(resp[k] != null && k != "id"){
                    if(k == "doc_num"){
                        $(".own-write-tab input[name='"+k+"_update']").val(resp[k]);
                    }else{
                        $(".own-write-tab input[name='"+k+"']").val(resp[k]);
                    }
                }
            }
        },
        error: function(jqXHR, txt, status){
            if(jqXHR.status == 401 || jqXHR.status == 403){
                alert(jqXHR.responseJSON.detail);
                window.open("/", "_self");
            }else{
                alert(jqXHR.responseJSON.detail);
            }
        }
    });
}

// 修改/编辑
function edit_account(cur_id) {
    clear_form();

    show_add_info("edit");
    form_info(cur_id);
}