from django.core.management.base import BaseCommand

from django.contrib.auth.models import Group

from django.conf import settings

from import_data.models import Account


class Command(BaseCommand):

    help = 'Add info to account'

    def add_arguments(self, parser):
        parser.add_argument('-f',
                            '--file_path',
                            required=True,
                            help='文件路径(绝对路径)')
        parser.add_argument('-s',
                            '--start_row',
                            default=4,
                            required=False, help='起始行')
        parser.add_argument('-e',
                            '--end_row',
                            default=250,
                            required=False, help='终止行')

    def a(self, sheet_one, cr):
        serial = sheet_one['{}{}'.format('A', cr)].value
        num_serial = sheet_one['{}{}'.format('B', cr)].value
        doc_num = sheet_one['{}{}'.format('C', cr)].value
        doc_index = sheet_one['{}{}'.format('D', cr)].value
        title = sheet_one['{}{}'.format('E', cr)].value
        subtitle = sheet_one['{}{}'.format('F', cr)].value
        file_type = sheet_one['{}{}'.format('G', cr)].value
        op_site = sheet_one['{}{}'.format('H', cr)].value
        file_main = sheet_one['{}{}'.format('I', cr)].value
        start_time = sheet_one['{}{}'.format('J', cr)].value
        end_time = sheet_one['{}{}'.format('K', cr)].value
        arch_department = sheet_one['{}{}'.format('L', cr)].value
        arch_time = sheet_one['{}{}'.format('M', cr)].value
        resp_person = sheet_one['{}{}'.format('N', cr)].value
        page_num = sheet_one['{}{}'.format('O', cr)].value
        file_content = sheet_one['{}{}'.format('P', cr)].value
        application_num = sheet_one['{}{}'.format('Q', cr)].value
        file_status = sheet_one['{}{}'.format('R', cr)].value
        file_secret = sheet_one['{}{}'.format('S', cr)].value
        time_long = sheet_one['{}{}'.format('T', cr)].value
        good_day = sheet_one['{}{}'.format('U', cr)].value
        is_origin = sheet_one['{}{}'.format('V', cr)].value
        text_msg = sheet_one['{}{}'.format('W', cr)].value
        apply_time = sheet_one['{}{}'.format('X', cr)].value

        try:
            account = Account()
            account.serial = serial
            account.num_serial = num_serial
            account.doc_index = doc_index
            account.doc_num = doc_num
            account.title = title
            account.subtitle = subtitle
            account.file_content = file_content
            account.file_main = file_main
            account.file_secret = file_secret
            account.file_status = file_status
            account.file_type = file_type
            account.op_site = op_site
            account.start_time = start_time
            account.end_time = end_time
            account.arch_department = arch_department
            account.arch_time = arch_time
            account.resp_person = resp_person
            account.page_num = page_num
            account.application_num = application_num
            account.time_long = time_long
            account.good_day = good_day
            account.is_origin = is_origin
            account.text_msg = text_msg
            account.apply_time = apply_time
            account.save()
        except Exception as e:
            raise e

    def handle(self, *args, **options):
        filename = options.get('file_path')

        import openpyxl

        wb = openpyxl.load_workbook(filename)

        sheetsname = wb.sheetnames

        # 只有一个表格的情况下
        sheet_one = wb[sheetsname[0]]

        for cur_row in range(4, 251):

            self.a(sheet_one, cur_row)
