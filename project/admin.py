from django.contrib import admin
from .models import Account

# Register your models here.


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('doc_num', 'doc_index', 'title', 'op_site', 'start_time')

    list_filter = ('doc_num', 'title', 'start_time')

    search_fields = ('op_site', 'file_main', 'start_time', 'title', 'file_type', 'arch_department')
