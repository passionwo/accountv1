from django.db import models


class Account(models.Model):
    owner = models.ForeignKey('user.MyUser',
                              on_delete=models.CASCADE)
    create_time = models.DateTimeField(verbose_name='创建时间',
                                       help_text='创建时间',
                                       # default='',
                                       auto_now_add=True)
    doc_num = models.CharField(verbose_name='档案编号',
                               help_text='档案编号',
                               null=True,
                               blank=True,
                               max_length=50)
    doc_index = models.CharField(verbose_name='档案位置',
                                 help_text='档案位置',
                                 null=True,
                                 blank=True,
                                 max_length=50)
    title = models.CharField(verbose_name='文件题名',
                             help_text='文件题名',
                             null=True,
                             blank=True,
                             max_length=200)
    subtitle = models.CharField(verbose_name='标的',
                                help_text='标的',
                                null=True,
                                blank=True,
                                max_length=200)
    file_type = models.CharField(verbose_name='档案类型',
                                 help_text='档案类型',
                                 null=True,
                                 blank=True,
                                 max_length=100)
    op_site = models.CharField(verbose_name='责任者2',
                               help_text='责任者2',
                               null=True,
                               blank=True,
                               max_length=100)
    file_main = models.CharField(verbose_name='责任者1',
                                 help_text='责任者1',
                                 null=True,
                                 blank=True,
                                 max_length=100)
    start_time = models.DateField(verbose_name='文件日期',
                                  help_text='文件日期',
                                  blank=True,
                                  null=True)
    end_time = models.DateField(verbose_name='到期时间',
                                help_text='到期时间',
                                blank=True,
                                null=True)
    arch_department = models.CharField(verbose_name='归档部门',
                                       help_text='归档部门',
                                       null=True,
                                       blank=True,
                                       max_length=50)
    arch_time = models.DateField(verbose_name='归档时间',
                                 help_text='归档时间',
                                 blank=True,
                                 null=True)
    resp_person = models.CharField(verbose_name='归档人',
                                   help_text='归档人',
                                   null=True,
                                   blank=True,
                                   max_length=30)
    page_num = models.IntegerField(verbose_name='页数',
                                   blank=True,
                                   null=True,
                                   help_text='页数')
    file_content = models.CharField(verbose_name='概述',
                                    help_text='概述',
                                    max_length=200,
                                    blank=True,
                                    null=True)
    # file_status = models.CharField(verbose_name='状态',
    #                                help_text='状态',
    #                                max_length=10,
    #                                blank=True,
    #                                null=True)
    file_secret = models.CharField(verbose_name='密级',
                                   help_text='密级',
                                   blank=True,
                                   max_length=10,
                                   null=True)
    time_long = models.CharField(verbose_name='保管期限',
                                 help_text='保管期限',
                                 blank=True,
                                 null=True,
                                 max_length=50)
    # good_day = models.CharField(verbose_name='有效保管天数',
    #                             help_text='有效保管天数',
    #                             blank=True,
    #                             null=True,
    #                             max_length=10)
    is_origin = models.CharField(verbose_name='原件',
                                 help_text='原件',
                                 blank=True,
                                 null=True,
                                 max_length=10)
    text_msg = models.CharField(verbose_name='备注',
                                help_text='备注',
                                blank=True,
                                null=True,
                                max_length=200)
    # apply_time = models.DateField(verbose_name='申请时间',
    #                               help_text='申请时间',
    #                               blank=True,
    #                               null=True)

    def __str__(self):
        return '%s' % self.doc_num

    class Meta:
        verbose_name = '台账信息'
        ordering = ('-create_time', '-pk')
        permissions = (
            ("project_normal", "project_normal_permissions"),
            ('project_high', 'project_high_permissions'),
        )

        verbose_name_plural = verbose_name
