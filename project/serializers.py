from rest_framework import serializers

from .models import Account


class DetailInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = '__all__'

    def create(self, validated_data):
        return Account.objects.create(**validated_data)


class InfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = '__all__'
        read_only_fields = ('doc_num',
                            'file_type',
                            'title',
                            'file_main',
                            'op_site',
                            'start_time',
                            'page_num')
        extra_kwargs = {
            'doc_index': {'write_only': True},
            'subtitle': {'write_only': True},
            'end_time': {'write_only': True},
            'arch_department': {'write_only': True},
            'arch_time': {'write_only': True},
            'resp_person': {'write_only': True},
            'file_content': {'write_only': True},
            'file_secret': {'write_only': True},
            'time_long': {'write_only': True},
            'is_origin': {'write_only': True},
            'text_msg': {'write_only': True},
        }
