from django.urls import path

from .views import (InfoView,
                    UploadFileView,
                    IndexInfoView,
                    FileOutView)

urlpatterns = [

    path('index/',
         IndexInfoView.as_view({'get': 'index'}),
         name='info-index'),
    path('detail_info/<int:pk>/',
         IndexInfoView.as_view({'get': 'detail_info'}),
         name='info-detail'),
    path('info/',
         InfoView.as_view({'get': 'list'}),
         name='info-list'),
    path('info/<int:page>/',
         InfoView.as_view({'get': 'list'}),
         name='info-list'),

    path('createinfo/',
         InfoView.as_view({'post': 'createinfo'}),
         name='create-info'),

    path('select_info/',
         InfoView.as_view({'post': 'select_info'}),
         name='select-info'),
    path('select_info/<int:page>/',
         InfoView.as_view({'post': 'select_info'}),
         name='select-info'),

    path('info/<int:pk>/info_detail/',
         InfoView.as_view({'get': 'info_detail'}),
         name='info-pk-detail'),
    path('info/<int:pk>/info_delete/',
         InfoView.as_view({'get': 'info_delete'}),
         name='info-delete'),

    path('upload/', UploadFileView.as_view({'post': 'post'}), name='upload'),

    path('export/', FileOutView.as_view({'get': 'list'}), name='export'),

]
