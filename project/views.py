from rest_framework import status

from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets

from django.core.paginator import Paginator
from django.conf import settings
from django.db.models import Q

from .models import Account
from .constants import info_load, error_info
from .permissions import IsUserPermission
from .serializers import InfoSerializer, DetailInfoSerializer

from operator import and_
from functools import reduce
import pandas as pd
import copy


class IndexInfoView(viewsets.ModelViewSet):
    serializer_class = InfoSerializer
    renderer_classes = [TemplateHTMLRenderer]
    permission_classes = []
    authentication_classes = []

    @action(detail=False)
    def index(self, request, *args, **kwargs):
        return Response(template_name='index.html')

    @action(detail=False)
    def detail_info(self, request, *args, **kwargs):
        return Response(template_name='detail.html')


class InfoView(viewsets.ModelViewSet):
    queryset = Account.objects.all().order_by('-create_time', '-id')
    serializer_class = DetailInfoSerializer
    renderer_classes = [JSONRenderer, ]
    permission_classes = [IsUserPermission]

    def return_json(self, cur_page=None, tps=None, data=None):
        error_data = {}

        if tps == 'search':
            results = data
        else:
            results = self.get_queryset()

        serialzer = self.get_serializer(results,
                                        many=True)

        try:
            self.request.session['%s' % self.request.user.id] = serialzer.data
        except Exception:
            pass

        cnt = Paginator(serialzer.data, 10)

        total_pages = cnt.num_pages

        if cur_page > total_pages:
            cur_page = total_pages

        info_data_page = cnt.get_page(cur_page)

        is_next = info_data_page.has_next()

        is_prev = info_data_page.has_previous()

        next_page_num = None
        prev_page_num = None

        if is_next:
            try:
                next_page_num = info_data_page.next_page_number()
            except Exception:
                error_data['detail'] = '下一页不存在...'
                return Response(error_data, status=status.HTTP_400_BAD_REQUEST)
        if is_prev:
            try:
                prev_page_num = info_data_page.previous_page_number()
            except Exception:
                error_data['detail'] = '上一页不存在...'
                return Response(error_data, status=status.HTTP_400_BAD_REQUEST)

        user = self.request.auth.user
        is_has_del_permission = user.has_perm('project.project_high')
        resp_data = {
            'title': '台账信息',
            'list_of_info': info_data_page.object_list,
            'cur_page': cur_page,
            'total_pages': total_pages,
            'next_page_num': next_page_num,
            'prev_page_num': prev_page_num,
            'is_next': is_next,
            'is_prev': is_prev,
            'username': user.user_name,
            'has_del_permission': is_has_del_permission,
        }
        return resp_data

    @action(detail=False)
    def info_detail(self, request, pk, *args, **kwargs):

        response_data = {}
        try:
            info_obj = self.get_queryset().get(pk=pk)
        except Account.DoesNotExist:
            response_data['detail'] = '改记录已被删除, 无法查看'
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
        serialzer = DetailInfoSerializer(info_obj)
        return Response(serialzer.data)

    @action(detail=False)
    def info_delete(self, request, pk, *args, **kwargs):
        resp_data = {'detail': ''}

        try:
            info_pk_obj = self.get_queryset().get(pk=pk)
        except Account.DoesNotExist:
            resp_data['detail'] = '此条信息已经不存在...'
            return Response(resp_data)

        try:
            info_pk_obj.delete()
        except Exception:
            resp_data['detail'] = '删除操作出现错误...'
            return Response(resp_data)

        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['POST'])
    def createinfo(self, request, *args, **kwargs):
        resp_data = {}
        user = request.auth.user

        if not user:
            resp_data['detail'] = '用户未登录, 请先登录...'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        data = request.data
        doc_num_create = data.get('doc_num_create', None)
        doc_num_update = data.get('doc_num_update', None)
        info_object = copy.deepcopy(info_load)

        doc_num = doc_num_create if doc_num_create else doc_num_update

        exist_obj = None
        if doc_num:
            exist_obj = self.get_queryset().filter(doc_num=doc_num)
        else:
            resp_data['detail'] = '表格中"编号"不可为空...'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        for key in info_load.keys():
            if data.get(key):
                info_object[key] = data.get(key)
            else:
                info_object[key] = None

        info_object.update({"owner": user.id, 'doc_num': doc_num})

        if exist_obj:
            serializer = DetailInfoSerializer(exist_obj[0], data=info_object, partial='partial')
            try:
                serializer.is_valid(raise_exception=True)
                self.perform_update(serializer)
            except Exception:
                resp_data['detail'] = error_info.get(list(serializer.errors.keys())[0])
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

            resp_data['detail'] = '修改成功'
            return Response(resp_data)

        try:
            account = DetailInfoSerializer(data=info_object)
            if account.is_valid():
                account.save()
            else:
                resp_data['detail'] = error_info.get(list(account.errors.keys())[0])
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            resp_data['detail'] = '保存失败, 请重试... %s' % e
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        resp_data['detail'] = '添加成功'
        return Response(resp_data)

    @action(detail=True, methods=['POST'])
    def select_info(self, request, *args, **kwargs):
        resp_data = {}
        req_data = request.data
        current_page = kwargs.get('page', 1)
        start_time = req_data.get('s_time', None)
        end_time = req_data.get('e_time', None)
        use_key = [
            key
            for key in req_data.keys()
            if key in Account.__dict__.keys()
        ]

        is_all_none = []
        is_all_none.append(start_time)
        is_all_none.append(end_time)

        for m in use_key:
            is_all_none.append(req_data.get(m))

        if not any(is_all_none):
            resp_obj = self.get_queryset().all()
        else:
            queries = []
            for k in use_key:
                value = req_data.get(k)
                if value:
                    query = '__'.join([k, 'icontains'])
                    queries.append(Q(**{query: value}))

            if queries:
                query_results = self.get_queryset().filter(reduce(and_, queries))
            else:
                query_results = self.get_queryset()

            try:
                if start_time and not end_time:
                    resp_obj = query_results.filter(start_time__gte=start_time)
                elif end_time and not start_time:
                    resp_obj = query_results.filter(start_time__lte=end_time)
                elif all([start_time, end_time]):
                    resp_obj = query_results.filter(start_time__range=(start_time, end_time))
                else:
                    resp_obj = query_results
            except Exception as e:
                resp_data['detail'] = e
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        resp_data = self.return_json(cur_page=current_page, tps='search', data=resp_obj)

        return Response(resp_data)

    def list(self, request, *args, **kwargs):
        cur_page = kwargs.get('page', 1)
        resp_data = self.return_json(cur_page=cur_page)
        return Response(resp_data)


class UploadFileView(viewsets.ModelViewSet):
    renderer_classes = [JSONRenderer, TemplateHTMLRenderer]
    queryset = Account.objects.all().order_by('-create_time', '-id')
    serializer_class = InfoSerializer
    permission_classes = [IsUserPermission, ]

    def return_json(self, cur_page=None, tps=None, data=None):
        return InfoView.return_json(self, cur_page=cur_page, tps=tps, data=data)

    def analyse(self, row, load_data, update_num, create_num):

        try:
            account_obj = Account.objects.filter(doc_num=load_data.get('doc_num'))
        except Exception as e:
            raise ValueError(e)

        load_data.update({'owner': self.request.user})

        if account_obj:
            account_obj.update(**load_data)
            update_num += 1
        else:
            try:
                account = Account(**load_data)
                account.save()
                create_num += 1
            except Exception as e:
                raise ValueError('%s' % e)

        return (update_num, create_num)

    def post(self, request, *args, **kwargs):
        resp_data = {}
        file_stream = request.data.get('uploadfile')
        start_row = request.data.get('start')

        if not file_stream:
            resp_data['detail'] = '请选择上传的文件!'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        try:
            if len(start_row) < 4:
                start_row = int(start_row)
            else:
                resp_data['detail'] = '输入数字过长'
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)
        except Exception:
            resp_data['detail'] = '"文件有效数据起始行"未输入...'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        file_path = '/'.join((settings.BASE_DIR, 'file.xlsx'))  # breakpoint

        with open(file_path, 'wb') as file:
            file.write(file_stream.read())

        try:
            res = pd.read_excel(file_path, index_col=None, header=None)
        except Exception:
            resp_data['detail'] = '文件必须为 xls/xlsx 表格类型'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        load_data = {}
        # -- 读取数据库的字典key
        zh_row = start_row - 2

        zh_idx = {}
        zh_to_name = {}

        for k, v in info_load.items():
            zh_to_name.update({v: k})

        try:
            for col in res.columns:
                va = res.iat[zh_row, col]
                if not pd.isna(va):
                    if va.strip() in zh_to_name.keys():
                        zh_idx.update({col: zh_to_name.get(va.strip())})
        except Exception:
            resp_data['detail'] = '查看有效行是否输入正确'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        if len(zh_idx) != len(info_load):
            resp_data['detail'] = '查看上传文件是否符合模板'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        # -- 读取完成
        update_num = 0
        create_num = 0

        # read row
        for row in res.index[start_row - 1:]:
            # read col
            for col in zh_idx.keys():
                val = res.iat[row, col]
                if pd.isna(val):
                    load_data.update({zh_idx.get(col): None})
                else:
                    load_data.update({zh_idx.get(col): val})

            if load_data:
                try:
                    update_num, create_num = self.analyse(row, load_data, update_num, create_num)
                except Exception:
                    resp_data['detail'] = '上传文件出现问题, 请检查文件后重试...'
                    return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)
            else:
                resp_data['detail'] = '信息读取失败,请重新上传'
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)
        # resp_content = self.return_json(cur_page=1)
        resp_data['detail'] = '上传成功:更新%s条记录;创建%s条记录.' % (update_num, create_num)
        return Response(resp_data)


class FileOutView(viewsets.ModelViewSet):
    serializer_class = DetailInfoSerializer
    renderer_classes = [JSONRenderer, ]
    permission_classes = [IsUserPermission]
    queryset = Account.objects.all()

    def save_file(self, value, ws, row, col_head):
        for k, v in info_load.items():

            ws.write(row, col_head.get(k), value.get(k))

    def list(self, request, *args, **kwargs):

        data = {}

        try:
            data = request.session['%s' % request.user.id]
        except Exception:
            pass

        writer = pd.ExcelWriter('%s/file_download/a.xlsx' %
                                settings.BASE_DIR, engine='xlsxwriter')
        df = pd.DataFrame()
        df.to_excel(writer, sheet_name='account', index=False)
        ws = writer.sheets['account']

        col_index = 0
        row_index = 0
        col_head = {}

        # 表头
        for k, v in info_load.items():
            ws.write(row_index, col_index, v)
            col_head[k] = col_index
            col_index += 1

        try:
            # 内容
            for v in data:
                row_index += 1
                self.save_file(v, ws, row_index, col_head)

            writer.save()
        except Exception:
            return Response({'detail': '导出文件失败'}, status=status.HTTP_400_BAD_REQUEST)

        return Response({'detail': '/download/a.xlsx'})
