from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import MyUser


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=u'密码', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'再次输入密码', widget=forms.PasswordInput)

    class Meta:
        model = MyUser
        fields = ('email', 'user_name', 'phone', 'password')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(u"密码输入不一致！")
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=("密码"),
                                         help_text=(
        '忘记密码 --> <a href={} '
        'style="padding: 5px;font-size: 14px;color:white;background: #84adc6;"'
        '>修改密码</a>'.format('../password/')
    ),)

    class Meta:
        model = MyUser
        fields = ('email', 'user_name', 'phone', 'password', 'is_active', 'is_admin')

    def clean_password(self):
        return self.initial["password"]


class MyUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('pk', 'email', 'user_name', 'phone',
                    'sex', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password', 'pwd_code')}),
        ('Personal info', {'fields': ('user_name', 'phone', 'sex')}),
        ('Permissions', {'fields': ('is_admin', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'user_name', 'phone', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'user_name', 'phone', 'pwd_code')
    ordering = ('pk', 'email',)
    filter_horizontal = ('user_permissions', 'groups')


admin.site.register(MyUser, MyUserAdmin)
