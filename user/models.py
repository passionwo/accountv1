from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.contrib.auth import authenticate

# Create your models here.


class MyUserManager(BaseUserManager):

    def create_user(self, email, user_name, phone, password=None):
        """
        Creates and saves a User with the given email, and password.
        """
        if not email:
            raise ValueError(u'用户需要以电子邮箱来创建！')

        user = self.model(
            email=self.normalize_email(email),
            user_name=user_name,
            phone=phone,
        )
        user.is_superuser = False
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, user_name, phone, password):
        """
        Creates and saves a superuser with the given email, and password.
        """
        user = self.create_user(email,
                                user_name=user_name,
                                phone=phone,
                                password=password
                                )
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    # 修改密码
    def db_change_password(self, loginEmail, oldPassword, newPassword):
        user = authenticate(email=loginEmail, password=oldPassword)
        if user is not None:
            if user.is_active:
                user.set_password(newPassword)
                user.save()
                return 1  # 修改成功，允许特殊符号
            else:
                return -2  # 没有权限
        else:
            return -1  # 旧密码错误


class MyUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name=u'电子邮箱',
        max_length=255,
        unique=True,
        help_text="电子邮箱",
    )
    phone = models.CharField(
        verbose_name=u'手机号',
        max_length=20,
        unique=True,
        help_text="手机号",
    )
    user_name = models.CharField(u'姓名', max_length=50, default='', help_text="用户名")
    department = models.CharField(u'所在部门', max_length=100, blank=True, help_text="医院")

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    SEX_CHOICES = (
        ('男', '男'),
        ('女', '女'),
        ('未知', '未知')
    )

    sex = models.CharField(verbose_name='性别',
                           max_length=2,
                           default='未知',
                           choices=SEX_CHOICES,
                           help_text='性别')

    pwd_code = models.CharField(verbose_name='修改密码验证信息',
                                max_length=6,
                                default='123456',
                                help_text='修改密码验证信息')

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['user_name', 'phone']

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):
        return self.email

    def is_staff(self):
        return True

    class Meta:
        verbose_name = u'用户信息'
        permissions = (
            ("user_operation", "user_all_permissions"),
        )
        verbose_name_plural = verbose_name
