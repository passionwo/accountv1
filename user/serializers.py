from rest_framework import serializers
from .models import MyUser

from django.contrib.auth.hashers import make_password


class LoginSerializer(serializers.Serializer):
    default_error_messages = {
        'name_pwd_none': '账号或密码未填写',
        'name_pwd_wrong': '账号或密码错误',
    }

    password = serializers.CharField(
        error_messages={'blank': default_error_messages['name_pwd_none']})
    email = serializers.CharField(
        error_messages={'blank': default_error_messages['name_pwd_none']})

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')
        if not all([email, password]):
            self.fail('name_pwd_none')

        try:
            user = MyUser.objects.get(email=email)
        except MyUser.DoesNotExist:
            self.fail('name_pwd_wrong')

        if user:
            is_right = user.check_password(password)

        if not is_right:
            self.fail('name_pwd_wrong')

        data['user'] = user

        return data


class ChangePwdSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    code = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
    verify_password = serializers.CharField(required=True)

    def validate(self, data):
        email = data.get('email')
        new_password = data.get('new_password')
        verify_password = data.get('verify_password')
        code = data.get('code')

        if not all([email, new_password, verify_password, code]):
            raise ValueError('信息填写不完全')

        try:
            user = MyUser.objects.get(email=email)
        except MyUser.DoesNotExist:
            raise ValueError('该用户不存在')

        if not user:
            raise ValueError('该用户不存在')

        u_code = user.pwd_code
        if u_code.lower() != code.lower():
            raise ValueError('验证码填写错误')

        if new_password != verify_password:
            raise ValueError('两次输入密码不一致')

        user.password = make_password(new_password)
        try:
            user.save()
        except Exception:
            raise ValueError('更改密码失败')

        return data
