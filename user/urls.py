from django.urls import path
from .views import LoginIndexView
from .views import ChangePasswordView

urlpatterns = [
    path('', LoginIndexView.as_view(), name='login'),
    path('verify/<email>/', ChangePasswordView.as_view(), name='pwd-code'),
    path('password/', ChangePasswordView.as_view(), name='change-pwd'),
]
