def create_code():
    import random

    const = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    return_list = random.sample(const, k=6)

    code = ''.join(return_list)

    return code
