from django.contrib.auth.password_validation import MinimumLengthValidator


class MinMum(MinimumLengthValidator):
    def __init__(self, min_length=6):
        self.min_length = min_length
