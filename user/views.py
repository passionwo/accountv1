# from django.shortcuts import get_object_or_404
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from rest_framework import generics, mixins, views
from rest_framework import status

from .serializers import LoginSerializer, ChangePwdSerializer
from .models import MyUser

from django.core.mail import send_mail
from django.conf import settings


class LoginIndexView(mixins.CreateModelMixin, generics.GenericAPIView):
    renderer_classes = [JSONRenderer, TemplateHTMLRenderer]
    serializer_class = LoginSerializer
    permission_classes = []
    authentication_classes = []
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        return Response(template_name='login.html')

    def post(self, request, *args, **kwargs):
        form_data = request.data

        resp_data = {}
        fields = [i for i in form_data if i in MyUser().__dict__]

        data = {}

        for v in fields:
            data.update({v: form_data.get(v)})

        serializer = LoginSerializer(data=data)
        serializer.is_valid()
        if serializer.errors:
            resp_data['detail'] = '账号或密码错误'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        user = serializer.validated_data['user']

        if user:
            is_token = Token.objects.filter(user=user)

        if is_token:
            is_token.delete()

        token = Token.objects.create(user=user)

        resp_data['token'] = token.key

        return Response(resp_data)


class ChangePasswordView(views.APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = []
    authentication_classes = []

    def get(self, request, email, *args, **kwargs):
        resp_data = {}

        try:
            user = MyUser.objects.get(email=email)
        except MyUser.DoesNotExist:
            resp_data['detail'] = '此邮箱用户不存在...'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        if not user:
            resp_data['detail'] = '此邮箱用户不存在...'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        from .utils import create_code
        try:
            code = create_code()
        except Exception:
            resp_data['detail'] = '验证码生成错误...'
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        if code:
            user.pwd_code = code

            try:
                user.save()
            except Exception:
                resp_data['detail'] = '验证码保存错误...'
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

            try:
                # send email
                send_mail('汉典台账查询系统',
                          '',
                          settings.EMAIL_HOST_USER,
                          [email],
                          html_message='验证码是:%s' % code)
            except Exception:
                resp_data['detail'] = '验证码发送失败...'
                return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        resp_data = {}
        form_data = request.data
        serialzer = ChangePwdSerializer(data=form_data)
        try:
            serialzer.is_valid()
        except Exception as e:
            resp_data['detail'] = '%s' % e
            return Response(resp_data, status=status.HTTP_400_BAD_REQUEST)

        resp_data['detail'] = '密码修改成功!'
        return Response(resp_data, status=status.HTTP_200_OK)
